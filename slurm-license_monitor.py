#!/usr/bin/python
import os,sys
import time,datetime
import parse
import subprocess
import pyslurm
import logging
import threading
from apscheduler.schedulers.background import BackgroundScheduler
#Released under the Apache License 2.0
#Author: Geert Geurts <ggeurts@verweggistan.eu>
#Donated by Dalco AG
 
#GG: modules parse, pyslurm and APScheduler need to be install using pip

logging.basicConfig(filename='/var/log/license-monitor.log', format='%(asctime)s %(message)s')
LMUTIL="/usr/local/comsol53a/multiphysics/license/glnxa64/lmutil"
#GG: We need a lmutil binary for querying the license server...

#GG: next 12 lines to check if sched_interval has been set in slurm.conf. This to make certain to update used licenses info at least 3 times and to update slurm's reservations 2 times each scheduling interval.
slurmConf=open("/etc/slurm/slurm.conf").readlines()
for L in slurmConf:
        sched_interval=parse.parse('sched_interval={:w}',L)
        if sched_interval: #if sched_interval defined in slurm.conf, then don't continue looking any further.
                break

try: 
	sched_interval
except NameError:
        sched_interval=60 #if not defined, set default sched_interval

flex_query_interval=sched_interval/3
#query_lm(lmutilcmd)
#GG: globally define both lic_dict and the lic_dict_lock variables.
#GG: lic_dict is a dictionary containing ALL licenses served by ALL license servers in the form:
#GG:  {'LICENSENAME1': {'total': 2, 'used': 0},'LICENSENAME2': {'total': 2, 'used': 0}}

lic_dict={}
lic_dict_lock = threading.RLock()


def query_lm(lmutilcmd):
        """
        The query_lm takes as argument the shell command to query a flexlm license server.
        It updates the global variable lic_dict with all licenses available from the flexlm license server specified in argument.
        Example: query_lm("LM_LICENSE_FILE=1718@flexlm-server /usr/local/comsol53a/multiphysics/license/glnxa64/lmutil lmstat -a")
        """
        
        global lic_dict
        global lic_dict_lock
        proc = subprocess.Popen([lmutilcmd], stdout=subprocess.PIPE, shell=True)
        (out, err) = proc.communicate()
        text=out.split("\n")
        lic_dict_lock.acquire()
        #GG: first a try: finalize combination to make certain eventually the lock is released.
        try:
                for L in text:
                #GG: for each line in output see if it's equal to parse templates, where variables "{:w}, {:d} and {:d}" are returned in a tuple.
                        #GG: output of flexlm changes format when only 1 license is available... Because of that first try for only "1 template", else use "more template" 
                        try:
                                #print L.index("licenses")
                                T=parse.parse('Users of {:w}:  (Total of {:d} licenses issued;  Total of {:d} licenses in use)',L)
                        except ValueError:
                                T=parse.parse('Users of {:w}:  (Total of {:d} license issued;  Total of {:d} license in use)',L)
                        if T: #if parsing of line was successful then T is not False, and license is updated or added to global lic_dict dictionary   
                                #print T
                                lic_dict[T[0]]={'total':T[1], 'used':T[2]}
                        #print("==================")
                        #print(lic_dict)
        finally:   #GG: when finished, release lock to lic_dict global variable 
                lic_dict_lock.release()


def update_slurm_license_reservations():
        """
        update_slurm_license_reservations goes trough the list of licenses defined in slurm, extracts the server and license names, and compares the amount of licenses "used" according to the flexlm server, with the amount of licenses "in_use" according to slurm licenses.
        If the number of licenses "used" according to the flex_lm server is greater then the number of licenses "in_use" by slurm, then there is apparently an external use active.
        update_slurm_license_reservations() will create a reservation for the amount of externally used licenses.
        NOTE: The externally used licenses remain as free according to slurm, but are not useable by slurm. They are reserved for user root.
        """
        slic_dict=pyslurm.licenses().get()
        a = pyslurm.reservation()
        current_res_dict=a.get()
        #GG: Go trough list of licenses defined in slurm and extract server and license name.
        for slic in slic_dict:
                inx=slic.index("@")
                lic=slic[:inx]
                server=slic[inx+1:]
                #print("License:", lic)
                #print("real:",lic_dict[lic])
                #print("slurm:",slic_dict[lic+"@"+server])
                try : #GG: Because slurm doesn't support uppercase license names, first try to subtract licenses "in_use" according to slurm from the licenses "used" according to the flex-lm server
                        difference=lic_dict[lic]["used"]-slic_dict[lic+"@"+server]["in_use"]
                except KeyError: #GG: Otherwise try to substract the licensename in upper case 
                        difference=lic_dict[lic.upper()]["used"]-slic_dict[lic+"@"+server]["in_use"]
                if difference>0: #GG: When more licenses are in use according to the flex_lm license server 
                        #print(current_res_dict)
                        if current_res_dict.get("external_"+slic): #GG: If there already exists an "external_" prepended reservation for this license, then update the reservation. 
                                #print("updating license")
                                #update_res_dict = pyslurm.create_reservation_dict()
                                #update_res_dict["name"]="external_"+lic
                                #update_res_dict["end_time"]=int(time.time())+flex_query_interval
                                #print(update_res_dict)
                                #a.update(update_res_dict)
                                CMD="scontrol update reservation Reservation=external_"+lic+"@"+server+" Licenses="+lic+"@"+server+":"+str(difference)+" EndTime="+datetime.datetime.strftime(datetime.datetime.now()+datetime.timedelta(minutes = 1),"%Y-%m-%dT%H:%M:%S")
                                logging.warning("Updating reservation "+"external_"+lic+"@"+server+" using: "+CMD)
                                proc = subprocess.Popen([CMD], stdout=subprocess.PIPE, shell=True)
                                (out, err) = proc.communicate()
                        else: #GG: Otherwise create a new reservation
                                CMD="scontrol create reservation Reservation=external_"+lic+"@"+server+" Licenses="+lic+"@"+server+":"+str(difference)+" StartTime="+datetime.datetime.strftime(datetime.datetime.now(),"%Y-%m-%dT%H:%M:%S")+" duration=00:"+str(sched_interval/2)+" partition=cluster user=root flags=LICENSE_ONLY partition=cluster"
                                logging.warning("Creating reservation "+"external_"+lic+"@"+server+" using: "+CMD)
                                #print("About to issue:",CMD)
                                #ans = raw_input("Continue?[y/n]:")
                                #if ans =="y":
                                proc = subprocess.Popen([CMD], stdout=subprocess.PIPE, shell=True)
                                (out, err) = proc.communicate()
                                #print("out:",out)
                                #print("err:",err)



if __name__ == '__main__':
        scheduler = BackgroundScheduler()
        slic_dict=pyslurm.licenses().get()
        servers={}
        #GG: go through list of licenses defined in slurm and create a dictionary of {licenseserver:["license1","license2","licenseN"]}
        for slic in slic_dict:
                inx=slic.index("@")
                lic=slic[:inx]
                server=slic[inx+1:]
                if servers.has_key(server):
                        servers[server].append(lic)
                else:
                        servers[server]=[lic]
                
        for server in servers:
                #GG: create a scheduler job for each server to update the global lic_dict variable for licenses served by this licenseserver.
                ARGS="LM_LICENSE_FILE="+server+" "+LMUTIL+" lmstat -a"
                logging.warning("Added monitoring of server "+server+" for license(s): "+str(servers[server])+" "+"Using ARGS: "+ARGS)
                scheduler.add_job(query_lm, 'interval', args=[ARGS], seconds=sched_interval/3)
        scheduler.add_job(update_slurm_license_reservations, 'interval', seconds=sched_interval/2)
        scheduler.start()
while True:
        time.sleep(1)


