# slurm-license_monitor
As there is no integration of license server daemons inside SLURM, the licenses defined in SLURM must be exclusively accesible by SLURM. This means that licenses can only be used through SLURM.
The SLURM-license_monitor integrates license managers like Flexlm into SLURM, making SLURM aware of actual license usage.

As an example: When a user is testing some simulation on his workstation, and by this he is consuming a license, this license is no longer available to SLURM, but SLURM is not aware of this as SLURM assumes exclusive access to the defined licenses. This might lead to job crash as SLURMs idea of license usage is not per se  equal to the effective license usage at the license server. 
To overcome this the slurm-license_monitor creates reservations for the externally used licenses, making SLURM aware of external license usage.
This is done by querying the license server and creating/updating reservations for that license inside SLURM. 

Written by Geert Geurts, donated by Dalco AG.

## Prerequisites
- pip
- Cython
- slurm-devel
- python34-devel
- parse
- pyslurm 
- APScheduler

```
yum -y install python34-Cython slurm-devel python34-devel python34-pip
pip3 install parse pyslurm APScheduler
```
*For python2 just change the packages to their version 2 counterparts.*

### Installing
## 1) Installing the files.
- Copy the `slurm-license_monitor.py` python script to `/usr/local/sbin` and make certain it is executable(`chmod +x /usr/local/sbin/slurm-license_monitor.py`).
- Copy the `slurm-license_monitor.service` service file to `/etc/systemd/system/`.
- Modify the `LMUTIL` variable to the full path of a `lmutil` binary.
- Reload the systemd services by executing `systemctl daemon-reload`

## 2) Define the license
We need to define the licensename, lisencecount  and the license server in SLURM. This is done in the following format: 
```
root@master ~]# sacctmgr add resource name=comsol count=2 server=1718@flexlm-server  servertype=flexlm type=license
```
The values are extracted from the output of 'LM_LICENSE_FILE=1718@flexlm-server lmutil lmstat -a' converted to lowercase.

Example `lmutil lmstat -a` output line: `Users of COMSOL:  (Total of 2 licenses issued;  Total of 2 licenses in use)`
- "name" is the license name (`name=comsol`). 
- "count" is the number of licenses issued (`count=2`).
- "server" is the port@server where the lisence manager process is listening (`server=1718@flexlm-server`).

```
root@master ~]# sacctmgr show resource
      Name     Server     Type  Count % Allocated ServerType
---------- ---------- -------- ------ ----------- ----------
      comsol 1718@flexlm-server  License      2          0     flexlm
```

## 3) Starting slurm-license_monitor
```
root@master ~]# systemctl start slurm-license_monitor
root@master ~]# systemctl status slurm-license_monitor
```

## 4) Submitting a job
Jobs can be submitted using the following command:
```
ggeurts@master ~]$ sbatch -L comsol@1718@flexlm-server job.sh
```
When someone is running comsol on a workstation, outside of SLURM, and by that is consuming one comsol license, inside SLURM a reservation is created for an comsol license for the user root. 

### Debugging
When an license is being used outside of SLURM (someone starts a simulation interactively) a logline will be written to `/var/log/license-monitor.log`.

## Authors

* **Geert Geurts** 

## License

This project is licensed under the Apache License 2.0

## Acknowledgments

* This project has been donated by **Dalco AG**.
